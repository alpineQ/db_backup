package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/robfig/cron"
	"gitwork.ru/alpineQ/db_backup/cmd"
	"gitwork.ru/alpineQ/db_backup/config"
	"gitwork.ru/alpineQ/db_backup/web"
)

func main() {
	conf, err := config.Load("config.json")
	if err != nil {
		log.Panic(err)
		return
	}
	c := cron.New()
	for _, db := range conf.DBConfigs {
		c.AddFunc(db.BackupFreq, func() { cmd.Backup(db.Name, db.BackupCMD, db.MaxBackups) })
	}
	c.Start()
	defer c.Stop()

	r := mux.NewRouter()

	r.PathPrefix("/static/db_backup/").Handler(http.StripPrefix("/static/db_backup/", http.FileServer(http.Dir("web/static"))))
	r.HandleFunc("/db_backup/", web.IndexRoute)
	r.HandleFunc("/db_backup/{container_name}/backup/", web.BackupRoute).Methods("POST")
	r.HandleFunc("/db_backup/{container_name}/restore/", web.RestoreRoute).Methods("POST")
	r.HandleFunc("/db_backup/{container_name}/delete/", web.DeleteRoute).Methods("POST")
	http.Handle("/", r)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
