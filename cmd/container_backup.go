package cmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"

	docker "github.com/fsouza/go-dockerclient"
)

var client, _ = docker.NewClientFromEnv()

// Backup Создание резервной копии
func Backup(containerName string, backupCmd []string, MaxBackups int) error {
	containers, err := client.ListContainers(
		docker.ListContainersOptions{Filters: map[string][]string{"name": {containerName}}})

	if err != nil || len(containers) != 1 {
		return err
	}
	containerID := containers[0].ID
	backupName := time.Now().Format("2006-01-02-15:04")
	backupFilePath := "backups/" + containerName + "/" + backupName + ".tar"

	for index, elem := range backupCmd {
		if strings.Contains(elem, "$date") {
			backupCmd[index] = strings.Replace(backupCmd[index], "$date", backupName, -1)
		}
	}
	execInfo, err := client.CreateExec(docker.CreateExecOptions{
		AttachStderr: true,
		AttachStdout: true,
		AttachStdin:  true,
		Tty:          false,
		Cmd:          backupCmd,
		Container:    containerID,
	})
	if err != nil {
		return err
	}
	var buffer bytes.Buffer
	if err = client.StartExec(execInfo.ID, docker.StartExecOptions{
		OutputStream: &buffer,
		ErrorStream:  &buffer,
	}); err != nil {
		return err
	}
	fmt.Printf(buffer.String())

	if err := os.MkdirAll(filepath.Dir(backupFilePath), 0770); err != nil {
		return err
	}
	backupFile, err := os.Create(backupFilePath)
	if err != nil {
		return err
	}
	defer backupFile.Close()
	client.DownloadFromContainer(containerID, docker.DownloadFromContainerOptions{
		Path:         "/data/dump/" + backupName,
		OutputStream: backupFile,
	})

	files, err := ioutil.ReadDir("backups/" + containerName)
	if err != nil {
		log.Fatal(err)
	}
	sort.Slice(files, func(i, j int) bool {
		return files[i].ModTime().Before(files[j].ModTime())
	})
	for i := 0; i < len(files)-MaxBackups; i++ {
		if err := os.Remove("backups/" + containerName + "/" + files[i].Name()); err != nil {
			return err
		}
	}
	return nil
}

// Restore Восстановление резервной копии
func Restore(containerName string, restoreCmd []string, backupName string) error {
	containers, err := client.ListContainers(
		docker.ListContainersOptions{Filters: map[string][]string{
			"name": {containerName}}})
	if err != nil || len(containers) != 1 {
		log.Printf("Error: %s", err)
	}
	containerID := containers[0].ID

	for index, elem := range restoreCmd {
		if strings.Contains(elem, "$date") {
			restoreCmd[index] = strings.Replace(restoreCmd[index], "$date", backupName, -1)
		}
	}

	backupFile, err := os.Open("backups/" + containerName + "/" + backupName + ".tar")
	if err != nil {
		return err
	}
	defer backupFile.Close()
	client.UploadToContainer(containerID, docker.UploadToContainerOptions{
		Path:        "/data/dump/",
		InputStream: backupFile,
	})
	execInfo, err := client.CreateExec(docker.CreateExecOptions{
		AttachStderr: true,
		AttachStdout: true,
		AttachStdin:  true,
		Tty:          false,
		Cmd:          restoreCmd,
		Container:    containerID,
	})
	if err != nil {
		return err
	}
	var buffer bytes.Buffer
	if err = client.StartExec(execInfo.ID, docker.StartExecOptions{
		OutputStream: &buffer,
		ErrorStream:  &buffer,
	}); err != nil {
		return err
	}
	fmt.Printf(buffer.String())
	return nil
}
