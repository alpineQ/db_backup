package cmd

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/dgrijalva/jwt-go"
)

// GetAuthData Функция получения данных авторизации из запроса
func GetAuthData(jwtString string) (string, string) {
	token, err := jwt.Parse(jwtString, getKeyByID)
	if err != nil {
		log.Fatal(err)
		return "", ""
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return fmt.Sprintf("%v", claims["username"]), fmt.Sprintf("%v", claims["group"])
	}
	return "", ""
}

func getKeyByID(token *jwt.Token) (interface{}, error) {
	if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
		return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
	}
	keyID := token.Header["kid"]
	if keyID == "" {
		return "", errors.New("Invalid JWT")
	}
	authURL := os.Getenv("AUTH_URL")
	if authURL == "" {
		authURL = "https://sms.gitwork.ru/auth"
	}
	resp, err := http.Get(authURL + "/public_key/" + fmt.Sprintf("%v", keyID))
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	publicKey, err := jwt.ParseRSAPublicKeyFromPEM(body)
	if err != nil {
		return "", err
	}
	return publicKey, nil
}
