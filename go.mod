module gitwork.ru/alpineQ/db_backup

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fsouza/go-dockerclient v1.6.5
	github.com/gorilla/mux v1.8.0
	github.com/robfig/cron v1.2.0
)
