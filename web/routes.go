package web

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	docker "github.com/fsouza/go-dockerclient"
	"github.com/gorilla/mux"
	"gitwork.ru/alpineQ/db_backup/cmd"
	"gitwork.ru/alpineQ/db_backup/config"
)

var indexTemplate = template.Must(template.ParseFiles("web/templates/index.html"))

// IndexData Структура предоставляемая Index для сборки HTML страницы
type IndexData struct {
	DBInfos  []DBInfo
	Username string
	Group    string
}

// DBInfo Информация о конкретной БД
type DBInfo struct {
	Name    string
	Backups []string
	Status  string
}

// IndexRoute Основная страница приложения
func IndexRoute(w http.ResponseWriter, r *http.Request) {
	var dbInfos = []DBInfo{}
	containerFolders, err := ioutil.ReadDir("backups")
	if err != nil {
		log.Printf("Error: %s", err)
	}
	for _, dbConfig := range config.Config.DBConfigs {
		dbInfos = append(dbInfos, DBInfo{Name: dbConfig.Name, Status: "Down"})
		for _, containerFolder := range containerFolders {
			if !containerFolder.IsDir() {
				continue
			}
			if dbConfig.Name == containerFolder.Name() {
				backupFiles, err := ioutil.ReadDir("backups/" + containerFolder.Name())
				if err != nil {
					log.Printf("Error: %s", err)
				}
				var containerBackups []string
				for i := len(backupFiles) - 1; i >= 0; i-- {
					containerBackups = append(containerBackups, backupFiles[i].Name()[:len(backupFiles[i].Name())-4])
				}
				for dbIndex, db := range dbInfos {
					if db.Name == dbConfig.Name {
						dbInfos[dbIndex].Backups = containerBackups
						break
					}
				}
			}
		}
	}

	var client, _ = docker.NewClientFromEnv()
	containers, err := client.ListContainers(docker.ListContainersOptions{})
	if err != nil {
		log.Printf("Error: %s", err)
	}
	for _, container := range containers {
		for dbIndex, db := range dbInfos {
			if container.Names[0][1:] == db.Name {
				dbInfos[dbIndex].Status = container.Status
			}
		}
	}
	jwt, err := r.Cookie("user_jwt")
	if err != nil {
		log.Printf("Error: %s", err)
		http.Error(w, "No user_jwt cookie provided", http.StatusForbidden)
		return
	}
	username, group := cmd.GetAuthData(jwt.Value)

	indexTemplate.Execute(w, IndexData{DBInfos: dbInfos, Username: username, Group: group})
}

// BackupRoute End-point резервного копирования
func BackupRoute(w http.ResponseWriter, r *http.Request) {
	containerName := mux.Vars(r)["container_name"]
	for _, db := range config.Config.DBConfigs {
		if db.Name == containerName {
			if err := cmd.Backup(db.Name, db.BackupCMD, db.MaxBackups); err != nil {
				log.Printf("Error: %s", err)
				return
			}
			http.Redirect(w, r, "/db_backup/", http.StatusFound)
			return
		}
	}
	fmt.Fprintf(w, "DB not found!")
}

// RestoreRoute End-point восстановления резервной копии
func RestoreRoute(w http.ResponseWriter, r *http.Request) {
	containerName := mux.Vars(r)["container_name"]
	backupDate := r.FormValue("date")
	if backupDate == "" {
		http.Error(w, "Invalid request", 400)
		return
	}
	for _, db := range config.Config.DBConfigs {
		if db.Name == containerName {
			if err := cmd.Restore(db.Name, db.RestoreCMD, backupDate); err != nil {
				log.Printf("Error: %s", err)
				return
			}
			http.Redirect(w, r, "/db_backup/", http.StatusFound)
			return
		}
	}
	fmt.Fprintf(w, "DB not found!")
}

// DeleteRoute End-point удаления резервных копий
func DeleteRoute(w http.ResponseWriter, r *http.Request) {
	containerName := mux.Vars(r)["container_name"]
	backupDate := r.FormValue("date")
	if backupDate == "" {
		fmt.Fprint(w, "No data field in request")
		return
	}
	if err := os.Remove("backups/" + containerName + "/" + backupDate + ".tar"); err != nil {
		fmt.Fprint(w, err)
		return
	}
	http.Redirect(w, r, "/db_backup/", http.StatusFound)
}
