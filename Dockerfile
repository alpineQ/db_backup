FROM golang:1.15.1-alpine AS builder
WORKDIR /go/src/gitwork.ru/alpineQ/db_backup/


COPY go.mod go.sum ./
RUN go mod download && go mod verify
RUN apk add --no-cache ca-certificates && update-ca-certificates

COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o main 

FROM scratch
COPY --from=builder /go/src/gitwork.ru/alpineQ/db_backup/ /
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

EXPOSE 8080
ENTRYPOINT [ "/main" ] 
